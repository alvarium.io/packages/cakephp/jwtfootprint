<?php
namespace Alvarium\JWTFootprint\Controller;

use Cake\Event\Event;
use Cake\Core\Configure;
use Muffin\Footprint\Auth\FootprintAwareTrait;


trait JWTFootprintAwareTrait
{
    use FootprintAwareTrait {
        _circumventEventManager as _userCircumventEventManager;
        footprint as realFootprint;
    }

    public function footprint(Event $event)
    {
        self::realFootprint($event);

        $this->_listener->setConfig('optionKey', Configure::read('JWT.optionKey'));
    }

    protected function _circumventEventManager($method, $args = [])
    {
        if ($method === 'getEntityClass') {
            return $this->_userCircumventEventManager($method, $args);
        }

        list($jwt) = list($result) = $args;

        return new \Cake\ORM\Entity($jwt);
    }

    protected function initJwtAuth()
    {
        $this->loadComponent('Auth', [
            'storage' => 'Memory',
            'authenticate' => [
                'ADmad/JwtAuth.Jwt' => [
                    'queryDatasource' => false,
                    'allowedAlgs' => Configure::read('JWT.algos'),
                    'key' => Configure::read('JWT.public'),
                ],
            ],
            'unauthorizedRedirect' => false,
            'checkAuthIn' => 'Controller.initialize',
            'loginAction' => false,
        ]);
    }

    /**
     * Defined just in case you don't define it on your AppController (weird use-case)
     *
     * @inheritDoc
     */
    public function initialize()
    {
        parent::initialize();

        $this->initJwtAuth();
    }
}
