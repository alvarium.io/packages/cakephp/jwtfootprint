JWTFootprint plugin for CakePHP
===============================

This plugin is a merge between UseMuffin/Footprint and ADmad/cakephp-jwt-auth in order to get JWT token information decrypted and available in your models (as `$entity->_jwt`).

Installation
------------

You can install this plugin into your CakePHP application using [composer](https://getcomposer.org).

The recommended way to install composer packages is:

~~~bash
composer require alvarium/jwt-footprint
~~~

The plugin **does not need activation**. Just start using it:

~~~php
<?php
// src/Controller/AppController.php

namespace App\Controller;

use Cake\Controller\Controller;
use Alvarium\JWTFootprint\Controller\JWTFootprintAwareTrait;


class AppController extends Controller
{
    use JWTFootprintAwareTrait;
}
~~~


If you already have an `initialize` method defined in your `AppController` file, ensure you call the `initJwtAuth` method:

~~~php
<?php
// src/Controller/AppController.php

namespace App\Controller;

use Cake\Controller\Controller;
use Alvarium\JWTFootprint\Controller\JWTFootprintAwareTrait;


class AppController extends Controller
{
    use JWTFootprintAwareTrait;

    public function initialize()
    {
        parent::initialize();

        $this->initJwtAuth();
    }
}
~~~

After doing so, all requests containing an Authorization Bearer with a valid JWT will have that JWT payload available as `$options['_jwt']`:

~~~php
<?php

class WhateverTable extends Table
{
    public function beforeSave(\Cake\Event\Event $event, \Cake\ORM\Entity $entity, \ArrayObject $options)
    {
        debug($options['_jwt']);die;
    }
}
~~~
