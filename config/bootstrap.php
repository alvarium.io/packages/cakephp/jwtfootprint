<?php

use Cake\Core\Configure;

$jwt = [
    'public' => null,
    'algos' => ['RS256', 'HS256'],
    'optionKey' => '_jwt',
];

Configure::write('JWT', $jwt);

if (!defined('CONFIG')) {
    return;
}

if (file_exists(CONFIG . 'jwt.php')) {
    Configure::load('jwt');
}
